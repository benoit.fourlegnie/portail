---
title: Programmation modulaire
date: juin 2019
author: Équipe pédagogique DIU EIL Lille
geometry: width=18cm, height=25cm
graphics: oui
numbersections: oui
---

## Objectifs

-   Comprendre l'intérêt de la programmation modulaire
-   Savoir concevoir et utiliser un module

# Pourquoi la programmation modulaire ?

-   développement logiciel
-   modification et maintenance logicielle
-   réutilisabilité
-   création de nouveaux types de données

# Les modules en Python

Module = fichier contenant un ensemble de déclarations de fonctions ou
autres.

## Exemples de modules Python
Certains de ces modules sont des modules standard livrés avec toute
distribution de Python. D'autres doivent être installés indépendamment
(par exemple avec `pip` ou `conda`, ou via le gestionnaire de module de Thonny).

Modules standard

* `math` : constantes et fonctions mathématiques
* `random` : fonctions de génération d'alea
* `turtle` : dessiner avec une tortue
* `sys` : fonctions système

Modules non standard

* `PIL` : package de traitement d'images
* `networkx` : graphes
* `pylab` : package scientifique. Regroupe `numpy` (calculs numériques
  et vectoriels) et `matplotlib` (création de graphiques) 


## Importation d'un module

Plusieurs formes d'importation

-   `from <module> import *` 
-   `from <module> import <truc>`
-   `import <module>`
-   `import <module> as <autre nom>`

La différence entre la forme `from ... import ...` et la forme
`import ...` réside dans la forme à donner aux noms des fonctionnalités
offertes par le module :

-   avec la première forme, il suffit d'invoquer le simple nom de la
    fonctionnalité ;
-   avec la seconde forme, il est nécessaire de *pleinement qualifier*
    le nom en le préfixant du nom du module.

Voici quelques exemples classiques :

-   importation de toutes les fonctions définies dans le module `random` :

    ```python
    >>> from random import *
    >>> randint(0, 10)
    5
    >>> l = [1, 2, 3]
    >>> shuffle(l)
    >>> l
    [3, 1, 2]
    ```
  
-   importation de deux définitions du module `math`:

    ```python
    >>> from math import cos, pi
    >>> pi
    3.141592653589793
    >>> cos(pi)
    -1.0
    ```

-   forme `import math` :

    ```python    
    >>> import math
    >>> math.pi
    3.141592653589793
    >>> math.cos(math.pi)
    -1.0
    ```

-   forme `import random as alea` :

    ```python
    >>> import random as alea
    >>> alea.random(0, 10)
    8
    >>> l = [1, 2, 3]
    >>> alea.shuffle(l)
    >>> l
    [1, 3, 2]
    ```

## Utiliser un module en script

En Python, un module n'étant qu'un ensemble de déclarations faites
dans un fichier, il est possible d'utiliser un module en tant que
script et l'exécuter.

Si ce module ne contient que des définitions de constantes et fonctions,
son exécution ne produit rien.

En revanche, s'il contient des instructions de calculs et/ou
d'impressions, ces calculs et/ou impressions sont effectués.

C'est très bien lorsque le module est utilisé en tant que script
principal, mais cela peut être fâcheux à l'exécution d'un autre script
qui l'importe : des calculs inutiles peuvent être effectués et des
impressions inopportunes peuvent apparaître.

Une solution à cela consiste à placer dans le module toutes les
instructions à exécuter dans une instruction conditionnelle (placée en
général à la fin du texte)

```python
if __name__ == '__main__':
   # instructions à exécuter
```

La condition de cette instruction conditionnelle regarde si la variable
`__name__` vaut `'__main__'` ce qui est le cas uniquement si le module
est importé en tant que script principal.


**Attention :** il y a deux caractères blancs soulignés (underscore) entourant les mots
`name` et `main`.

On peut profiter de cette possibilité pour effectuer des doctests,
vérifiant la conformité du code aux spécifications décrites dans les
docstrings.

```python
if __name__ == '__main__':
   import doctest
   doctest.testmod()
```

Ainsi en phase de développement du module, il suffit d'exécuter le
module en tant que script principal pour effectuer ces tests. Bien
entendu, si le module est importé par un autre script, ces tests ne sont
pas exécutés.


## Exemple de conception de module : les nombres complexes


Pour illustrer notre propos, nous souhaitons réaliser un (petit) module
sur les nombres complexes.

**Remarque :**
les nombres complexes sont prédéfinis en Python. Pour écrire
littéralement le nombre complexe $a + ib$ en Python, on utilise la lettre
`j` pour désigner le nombre complexe $i$ (dont le carré vaut -1) en la
plaçant derrière la partie imaginaire $b$ sans espace.

```python
>>> z = 1 + 2j
>>> type(z)
<class 'complex'>
>>> z + z
(2+4j)
>>> z * z
(-3+4j)
```

Notre module sur les nombres complexes n'est donc présenté que pour des
raisons pédagogiques.


### Interface

Commençons par définir l'interface de ce module, c'est-à-dire
l'ensemble des fonctionnalités qu'il nous offre.

Chacune de ces fonctions sera décrite par une spécification précisant

-   son nom 
-   ses paramètres
-   la valeur qu'elle renvoie ou l'effet qu'elle produit
-   les contraintes d'utilisation (**UC** pour *Usage constraint*)
-   et éventuellement des exemples d'utilisation.

#### Constructeurs


Commençons par les constructeurs, autrement dit les fonctions permettant
de construire des nombres complexes à partir des types de base (`int` ou
`float`).

Ces constructeurs sont au nombre de deux :

* `create`
	    
    ```python
    create(real_part, imag_part)
    
    create a complex number with real part  and imaginary part 
    
    :param real_part: the real part of the complex number to create
    :type real_part: int or float
    :param imag_part: the imaginary part of the complex number to create
    :type real_part: int or float
    :return: the complex number real_part + i imag_part
    :rtype: complex
    :UC: none
    :Example:
    
    >>> z = create(1, 2)
    >>> get_real_part(z)
    1.0
    >>> get_imag_part(z)
    2.0
    ```
 

* `from_real_number`

    ```python
    from_real_number(x)
    
    create the complex number x + i0 from real number x
    
    :param x: a real number
    :type x: int or float
    :return: the complex number x + 0i
    :rtype: complex
    :UC: none
    :Example:
    
    >>> z = from_real_number(1)
    >>> get_real_part(z)
    1.0
    >>> get_imag_part(z)
    0.0
    ```

#### Sélecteurs

Passons aux sélecteurs.

* `get_real_part`

    ```python
    get_real_part(z)

	return the real part of complex number z
    
    :param z: a complex number
    :type z: complex
    :return: the real part of z
    :rtype: float
    :UC: none
    :Example:
    
    >>> z = create(1, 2)
    >>> get_real_part(z)
    1.0
    ```

* `get_imag_part`

    ```python
    get_imag_part(z)
    
	return the imaginary part of complex number z
    
    :param z: a complex number
    :type z: complex
    :return: the imaginary part of z
    :rtype: float
    :UC: none
    :Example:
    
    >>> z = create(1, 2)
    >>> get_imag_part(z)
    2.0
    ```

#### Comparaisons

Une fonction de comparaison permettant de tester l'égalité de deux
nombres complexes.

*  `are_equals`

    ```python
    are_equals(z1, z2)
    
    return True if complex numbers z1 and z2 are equals
           False otherwise
    
    :param z1: a complex number
    :type z1: complex
    :param z2: a complex number
    :type z2: complex
    :return: True if z1 = z2, False otherwise
    :rtype: bool
    :UC: none
    :Example:
    
    >>> z1 = create(1, 2)
    >>> z2 = create(1, 2)
    >>> z3 = create(1, -1)
    >>> are_equals(z1, z2)
    True
    >>> are_equals(z1, z3)
    False
    ```


#### Fonctions de calcul

Voici maintenant quelques fonctions de calculs sur les nombres
complexes.

* `modulus`

    ```python
    modulus(z)
    
    return the modulus of complex number z, ie :math:`\sqrt{x^2 + y^2}` 
           if :math:`z=x+yi`.
    
    :param z: a complex number
    :type z: complex
    :return: his modulus
    :rtype: float
    :UC: none
    :Example:
    
    >>> modulus(create(0, 0))
    0.0
    >>> modulus(create(3, 4))
    5.0
	```
	
* `add`

    ```python
    add(z1, z2)
    
    return the sum of the two complex numbers z1 and z2
    
    :param z1: a complex number
    :type z1: complex
    :param z2: a complex number
    :type z2: complex
    :return: z1 + z2
    :rtype: complex
    :UC: none
    :Example:
    
    >>> z = add(create(1, 2), create(3, 4))
    >>> get_real_part(z)
    4.0
    >>> get_imag_part(z)
    6.0
	```
	
* `mul`

    ```python
    mul(z1, z2)
    
    return the product of the two complex numbers z1 and z2
    
    :param z1: a complex number
    :type z1: complex
    :param z2: a complex number
    :type z2: complex
    :return: z1 * z2
    :rtype: complex
    :UC: none
    :Example:
    
    >>> z = mul(create(1, 2), create(3, 4))
    >>> get_real_part(z)
    -5.0
    >>> get_imag_part(z)
    10.0
	```

#### Imprimeur

Une fonction d'impression des nombres complexes dans une forme lisible
et compréhensible.

* `print`

    ```python
    print(z, end='\n')
    
    print the complex number z with algebraic form `x + yi`
          where x = real part of z and y = imaginary part
    
    :param z: complex number to print
    :type z: complex
    :param end: [optional] separator (default is '\\n')
    :type end: string
    :return: None
    :UC: none
    :Example:
    
    >>> z = create(1, 2)
    >>> print(z)
    1.000000 + 2.000000i
    ```

**Attention :** le choix du nom `print` est naturel pour une fonction d'impression.
Mais il faut bien prendre garde que si on utilise la forme
d'importation de module `from complex1 import *` alors toute référence
à `print` dans le script qui importe sera une référence à la fonction
`print` du module `complex1`. En particulier la fonction
`print` du langage de base ne sera plus accessible (sauf si on la
qualifie pleinement avec `builtins.print`).

### Une première réalisation

Passons maintenant à l'implémentation de ce module.

Une première possibilité pour représenter les nombres complexes est
d'utiliser les structures de dictionnaire. Ainsi, nous pouvons
envisager de représenter les nombres complexes avec des dictionnaires à
deux champs `'re'` pour la partie réelle et `'im'` pour la partie
imaginaire. Ainsi le nombre complexe $1+2i$ sera représenté par le
dictionnaire `{'re': 1, 'im':2}`.


Cf fichier [complex1.py](complex1.py).


### Une autre réalisation

Une autre possibilité pour représenter les nombres complexes est
d'utiliser les tuples. Ainsi, nous pouvons envisager de représenter les
nombres complexes avec des couples de deux nombres : par exemple, le
nombre complexe $1+2i$ sera représenté par le couple `(1,2)`.

Il est facile de réécrire les constructeurs et sélecteurs en tenant
compte de ce choix de représentation, **toutes les autres fonctions
restant inchangées**, grâce à l'usage exclusif des contructeurs et
sélecteurs.


Cependant un inconvénient des tuples par rapport aux dictionnaires réside dans le fait que écrire
`z[0]` est moins parlant que d'écrire `z['re']`.

Le [programme de la spécialité NSI pour la classe de première](https://cache.media.education.gouv.fr/file/SP1-MEN-22-1-2019/26/8/spe633_annexe_1063268.pdf) prévoit dans sa partie «Représentation des données : types construits) l'usage de p-uplets nommés (cf pages 4 et 5).
Il est précisé ensuite que ces $p$-uplets sont implémentés par des dictionnaires. 

Cependant Python offre la possibilité de créer des types de donnés à l'aide de $p$-iplets nommés : les `namedtuple` du module collections.
Le fichier [complex2.py](complex2.py) contient une seconde réalisation du module `complex` qui utilise cette possibilité.


### Un programme utilisant notre module

Le fichier [main.py](main.py) contient un programme qui

-   construit deux nombres complexes `z1` et `z2` à partir de quatre
    nombres passés sur la ligne de commande
-   les imprime ainsi que leur module
-   et imprime finalement leur somme et leur produit.


Voici deux traces de l'exécution de ce script dans un
terminal de commandes (le symbole `$` désigne l'invite de commande dans
un terminal de commandes, il ne faut pas l'écrire) :

* le programme est lancé en invoquant l'interpréteur du langage
  (commande `python3`) suivi du nom du script (`main.py`).

    ```console	
    $ python3 main.py
    Usage: main.py <x1> <y1> <x2> <y2>
    	<x1> <y1> : real and imaginary parts of a complex number
    	<x2> <y2> : real and imaginary parts of a second complex number
    ```
	
   Mais l'absence d'arguments supplémentaires sur la ligne de
   commande provoque l'appel à la fonction `usage` qui imprime l'usage correct 
   du programme.

* avec des arguments passés sur la ligne de commande

    ```console
    $ python3 main1.py 1 2 3 4
    z1 = 1.000000 + 2.000000i
    z1's module = 2.236068
    
    z2 = 3.000000 + 4.000000i
    z2's module = 5.000000
    
    z1 + z2 = 4.000000 + 6.000000i
    
    z1 * z2 = -5.000000 + 10.000000i
    ```

    Cette fois quatre nombres ont été fournis dans la ligne de commande,
    et notre programme fonctionne parfaitement.
