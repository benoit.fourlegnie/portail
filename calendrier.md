Calendrier 
==========

[DIU Enseigner l'informatique au lycée](./Readme.md)


jeudi 7 janvier 2021
====================

C'est la rentrée ! 

* 9h - « amphi » de rentrée  
  bâtiment M5 (modalités et précisions à venir)  
  Benoit Papegay et Philippe Marquet 
* 10h30-12h15 bloc 3  
  TP Prise en main de l'environnement de travail
* 13h45-17h bloc 1  
  Premiers travaux pratiques

> Accès au bâtiment M5 : [carte](https://osm.org/go/0B1fzL6bh--?m=) et [plan du campus](https://www.univ-lille.fr/fileadmin/user_upload/autres/Plan-site-Ulille-contact-cite%CC%81-scientifique.pdf)
> * accueil au bâtiment M5
> * les salles A11 à A16 se situent au 1er étage du M5
> * les salles A1 à A9 se situent au rdc du M5. 

vendredi 8 janvier 2021
=======================

* 9h-10h bloc 3  
  conférence _Turing, von Neumann, ... architecture des machines
  informatiques et des systèmes d'exploitation_  
  Gilles Grimaud

* 10h-12h15 bloc 2  
  tri  
  activité d'informatique sans ordinateur, travaux dirigés 

* 13h45-14h45 bloc 3  
  présentation « _Système d'exploitation — système de fichiers,
  processus, shell_ »  
  Philippe 
* 14h45-17h bloc 3  
  TP Initiation à UNIX, à l'interpréteur de commandes (suite et fin...)

mardi 12 janvier 2021
=====================

* 9h-10h bloc 1  
  présentation _Bonnes pratiques de programmation Python_
  Jean-Christophe Routier 
* 10h-12h15 bloc 1  
  Travaux pratiques

* 13h45-17h bloc 2  
  TD Synthèse sur les tris exprimés en Français  
  TP Analyse en temps d'exécution des tris

mercredi 13 janvier 2021
========================

* 9h-10h bloc 1  
  présentation _Récursivité_
  Jean-Christophe
* 10h-12h15 bloc 1  
  Récursivité  
  Travaux dirigés et activité sans ordinateur

* 13h45-14h45 bloc 2  
  présentation _Analyse théorique des algorithmes, illustration
  sur les algorithmes de tri_
  Benoit
* 14h45-17 bloc 2  
  Travaux dirigées Analyse de complexité d'algorithmes

jeudi 14 janvier 2021
=====================

* 9h-12h15 bloc 1  
  Récursivité puis lancment projet  
  Travaux pratiques

* 13h45-17h bloc 2  
  Complexité, utilisation de décorateurs  
  Travaux dirigés et pratiques
  

<!-- eof --> 
