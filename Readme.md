DIU Enseigner l'informatique au lycée
=====================================

Univ. Lille

Portail (public) du DIU Enseigner l'informatique au lycée, Université de Lille. Voir aussi http://portail.fil.univ-lille1.fr/fe/diu-eil

> Cette page d'accueil concerne plus particulièrement la promotion
2020/21-2021/22 du DIU.  
> La version précédente de la page d'accueil a été déplacée
à [Readme-1820.md](Readme-1820.md). 

Calendrier
----------

La formation découpée en cinq blocs se déroule sur deux années :

* blocs 1 à 3 enseignés l'année 2020/21
* blocs 4 et 5 enseignés l'année 2021/22

### → Année 2020/21 – blocs 1 à 3 ###

* **réunion de rentrée jeudi 7 janvier 2021, 9h**
  * désolé, pas de café d'accueil, contraintes sanitaires :mask:
    obligent :smirk: 
* cinq premières journées les 
  - jeudi 7 et vendredi 8 janvier 2021
  - mardi 12 au jeudi 14 janvier 2021
* a priori de 9h à 12h15 et de 13h45 à 17h

Voir la [page dédiée - calendrier](calendrier.md)

* bloc, horaire, salles, intervenants, etc. 


Groupes
-------

La promotion est divisée en 4 groupes

* voir la [page dédiée - groupes](groupes/Readme.md)

Informations pratiques
----------------------

* La formation se déroule à Villeneuve d'Ascq sur le campus cité
  scientifique de l'université de Lille.  
  Principalement au bâtiment M5. 
  - voyez [la carte](https://osm.org/go/0B1fzL6bh--?m=) 
  - ou le [plan du campus](https://www.univ-lille.fr/fileadmin/user_upload/autres/Plan-site-Ulille-contact-cite%CC%81-scientifique.pdf)

Contacts
--------

Responsables de la formation : Benoit Papegay et Philippe Marquet \
2e étage de l'extension du bâtiment M3 \
[📧 diu-eil@univ-lille.fr](mailto:diu-eil@univ-lille.fr)

Secrétariat pédagogique : Jessica Barret et Bruno Mahiddine \
rez-de-chaussée du bâtiment M3




